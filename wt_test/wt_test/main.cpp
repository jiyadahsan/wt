#include <Wt/WApplication.h>
#include <Wt/WBreak.h>
#include <Wt/WContainerWidget.h>
#include <Wt/WLineEdit.h>
#include <Wt/WPushButton.h>
#include <Wt/WText.h>
#include <Wt/WTemplate.h>
#include <Wt/WMessageResourceBundle.h>

class HelloApplication : public Wt::WApplication
{
public:
	HelloApplication(const Wt::WEnvironment& env);
	void on_submit_clicked();
	Wt::WLineEdit *email;
	Wt::WPushButton *submit;
private:
	Wt::WLineEdit *nameEdit_;
	Wt::WText *greeting_;
};

class login_form : public Wt::WTemplate
{
	
public:
	login_form(const Wt::WString& text) : Wt::WTemplate(text)
	{
		setTemplateText(Wt::WString::tr("login-form"));

		auto email = bindWidget("login-email", std::make_unique<Wt::WLineEdit>());
		email->setPlaceholderText("Email Address");
		auto password = bindWidget("login-password", std::make_unique<Wt::WLineEdit>());
		password->setPlaceholderText("Password");
		auto submit = bindWidget("login-submit", std::make_unique<Wt::WPushButton>("Login"));
		submit->clicked().connect(this, &HelloApplication::on_submit_clicked);
	}


};

HelloApplication::HelloApplication(const Wt::WEnvironment& env)
	: Wt::WApplication(env)
{
	// load the message resource bundle
	Wt::WApplication *app = Wt::WApplication::instance();
	app->messageResourceBundle().use("docroot/lang/lang");

	// Add an external style sheet to the application.
	app->useStyleSheet("style/base.css");	//Wt::WApplication::instance()->useStyleSheet("style/base.css");
	app->useStyleSheet("style/bootstrap/css/bootstrap.css");

	// Add an external javascript to the application.
	app->require("style/bootstrap/js/bootstrap.js");

	setTitle("WT Login Form");
	/*
	root()->addWidget(std::make_unique<Wt::WText>("Your name, please? "));
	nameEdit_ = root()->addWidget(std::make_unique<Wt::WLineEdit>());
	Wt::WPushButton *button = root()->addWidget(std::make_unique<Wt::WPushButton>("Greet me."));
	root()->addWidget(std::make_unique<Wt::WBreak>());
	greeting_ = root()->addWidget(std::make_unique<Wt::WText>());
	auto greet = [this] {
		greeting_->setText("Hello there, " + nameEdit_->text());
	};
	button->clicked().connect(greet);
	*/
	/*-------------- Containers -----------------*/
	
	/*auto container = std::make_unique<Wt::WContainerWidget>();
	root()->addWidget(std::move(container));
	container->setStyleClass("base");*/
	/*
	container->addWidget(std::make_unique<Wt::WBreak>());
	container->addWidget(std::make_unique<Wt::WBreak>());
	container->addWidget(std::make_unique<Wt::WText>("A first Widget"));
	

	for (unsigned int i = 0; i < 3; i++) {
		root()->addWidget(std::make_unique<Wt::WText>(Wt::WString("<h2>Text : {1}</h2>").arg(i)));
	}
	*/

	/*-------------- HTML Templates -----------------*/
   
	/*submit->clicked().connect([=] {
		submit->setText("Submit button Clicked");
	});*/

	/*
		t->bindWidget("name-edit", std::make_unique<Wt::WLineEdit>());
		t->bindWidget("save-button", std::make_unique<Wt::WPushButton>("Save"));
		t->bindWidget("cancel-button", std::make_unique<Wt::WPushButton>("Cancel"));
	*/

	auto tt = std::make_unique<login_form>(Wt::WString::tr("login-form"));
	root()->addWidget(std::move(tt));

	
	// resolve a string using the resource bundle
	/*auto welcome = std::make_unique<Wt::WText>(Wt::WString::tr("login-form").arg("ABC"));
	root()->addWidget(std::move(welcome));*/

}

void HelloApplication::on_submit_clicked()
{
	submit->setText("Submit Button Clicked");
	email->setText("abc@mail.com");
}


int main(int argc, char **argv)
{
	return Wt::WRun(argc, argv, [](const Wt::WEnvironment& env) {
		return std::make_unique<HelloApplication>(env);
	});
}

